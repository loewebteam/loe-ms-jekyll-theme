# loe-ms-jekyll-theme

version: 1.6.10

Logic of English Microsite theme.

----

## Installation

If you don't already have a Gemfile, run the following:

```ruby
bundle init
```

Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "loe-ms-jekyll-theme", :git => 'https://bitbucket.org/loewebteam/loe-ms-jekyll-theme.git'
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: loe-ms-jekyll-theme
```

And then execute:
```bash
bundle install
```
----

## New Sites

1. Run the following in Terminal to create a .ruby-version file:

        ruby -v > .ruby-version

2. Create a package.json and follow all the prompts

        npm init

3. Add all the dev packages we'll need for compiling javascript:

        npm install @babel/core @babel/preset-env babel-loader webpack webpack-cli webpack-merge --save-dev

4. Create a .gitignore containing the following:

        **node_modules
        .env
        public/*
        **.jekyll-cache
        src/assets/scripts/**
        
5. Add the following scripts to your package.json

```javascript
"scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "webpack-build-dev": "webpack --config webpack.dev.js",
    "webpack-watch-dev": "webpack --config webpack.dev.js --watch",
    "webpack-serve": "webpack-dev-server --config webpack.dev.js",
    "webpack-build-prod": "webpack --config webpack.prod.js",
    "prod-build": "webpack --config webpack.prod.js; jekyll build",
    "netdev": "netlify dev --port:4000 --dir:public",
    "netty-pot": "npm run netdev & npm run webpack-watch-dev"
  },
```

### _config.yml File for Jekyll

You'll need the following file in root for jekyll to run

```yaml
source: src
destination: public
keep_files: [assets]
permalink: :title
theme: loe-ms-jekyll-theme
compress_html:
  clippings: all
  comments: ["<!-- ", " -->"]
  ignore:
    envs: [local]
  blanklines: false
  profile: false
```

### Required directories

Create the following directories at root:
```bash
mkdir src
mkdir public
mkdir wksrc
mkdir wksrc/scripts
```

### Required files

```bash
touch wksrc/scripts/index.js
touch src/index.html
```

----

## Updates

From time to time, updates will be required. Run the following command from your command line editor.

```bash
bundle update loe-ms-jekyll-theme
```
After updating the bundle, and testing locally. Commit the gemfile.lock and any required updates to the repository. Ensuring the build is successful in production as well.

Any page using the default layout will have a hidden div below the footer with an attribute stating the version of this theme which is in use.

```html
<div style="display:none" loe-ms-jekyll-theme="0.3.5"></div> 
```

----

## Usage

Included in the theme are the following items

----
## Layouts 
folder: _layouts
### compress.html
takes the html, and minifies it
```yaml
layout: compress
```

### default.html
Has hooks for nav, content, css, and javascript. Use the following frontmatter to take advantage of the default layout
```yaml
layout: default
includeSEO: true
title: LOE TITLE
keywords: phonics, reading, spelling, handwriting, vocabulary, composition, grammar
description: Logic of English curriculum is designed to 
parentNavItem: products
css: home.css
loeJsWidgets: 
  - loe-buy-now
  - loe-scroller
  - loe-teacher-tip
  - loe-info-dialog
  - loe-expandi-box
  - loe-dialog
```
The "css" property is optional. Use it to specify the page-specific css (see the home.scss file below as a starting point). 

The loeJsWidgets property is also optional. Use this list to add javascript objects for use on the page.

----
## Data 
folder: _data
### sitemeta.yml
use this to set up the navigation for the site.
```yaml
title: 'SITE TITLE - sitemeta.yml'

#url will be the url of the current site when deployed in production.
url: "https://foundations.logicofenglish.com"

#links is the item used for navigation. 
# The most parently item, should be a site above this one in the hierarchy.
# the child with "/" for the url is the root of the current site.
links: 
    products:
      title: 'PARENT - yml'
      url: 'https://www.logicofenglish.com/products'
      children:
        foundations:
          title: 'Foundations'
          url: '/'
          children:
            a:
              title: 'A'
              url: '/a'
            b:
              title: 'B'
              url: '/b'
            c:
              title: 'C'
              url: '/c'
            d: 
              title: 'D'
              url: '/d'
        essentials:
          title: 'C-2'
          url: "https://essentials.logicofenglish.com"
        handwriting:
          title: 'C-3'
          url: 'https://rhythm-of-handwriting.logicofenglish.com'
        sos:
          title: "C-4"
          url: "https://sosw.logicofenglish.com"
        prodev:
          title: "C-5"
          url: "https://professional-development.logicofenglish.com"
```

----
## Sass (SCSS) 
folder: _sass

### _faq_block.scss
styling for the Knowledge base block, and questions to accompany a page
### _download-block.scss 
styling for the download icon with the text for the download
### _footer.scss 
the footer block of course
### _link-button.scss 
all of the buttons use this as a base
### _loe-defaults.scss
variables and mixins used throughout
### _mailchimp.scss
used in the footer
### _mobile-menu.scss
the full-screen menu used for tablet/mobile widths
### _nav-top.scss
white bar at top of each page
### _po-spot.scss
yellow block that links to the purchase order website
### _responsive-media.scss
responsive iframe, video holder, etc.
### _set-contents.scss
should probably be overridden, basic styling for the teacher and student items at the bottom of many product pages.
### _site-defaults.scss
starting point for taking variables and applying them to elements (_default is JUST scss variables)
### _sub-nav.scss
blue bar at top of each page.

----
## Assets
folder: assets

Putting scss files with frontmatter will automatically be converted to css in the site build.  
Reference other sass components in the as relative to the _sass folder.

```scss

---
# frontmatter, forces scss compiled to css
---

```


assets/styles:

### common.scss
starting point for items below the fold that could be used across multiple pages
### home.scss
home page css. contains the following items. use this as a basis for page-specific css
```scss
/* home.scss */
@import 'loe-defaults';
@import 'responsive-media';
@import 'link-button';
@import 'list-box';
@import 'faq-block';
@import 'download-block';
```

assets/scripts:

use webpack to build right into the scripts folder if custom javascript is required.

----

## Development
This theme uses rake and bump gems for development. When a new version is ready, and changes have been committed, run one of the following to ensure version info is updated in the gemspec, readme, and default layout, and any other file this gem version is used.

```bash
rake bump:major
```
```bash
rake bump:minor
```
```bash
rake bump:patch
```
If other text files should include the version number, update the Rakefile.