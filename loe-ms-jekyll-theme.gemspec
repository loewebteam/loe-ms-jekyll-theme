# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "loe-ms-jekyll-theme"
  spec.version       = "1.6.10"
  spec.authors       = ["Ethan Calvert"]
  spec.email         = [""]

  spec.summary       = "Logic of English Jekyll theme for microsites"
  spec.license       = "Nonstandard"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|_data|_config|index|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.0"
  spec.add_runtime_dependency "jekyll-algolia", "~> 1.6"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12.0"
end
