link row, not sure why... but should allow for future style changes.

```html
    <div class="link-row">
        <div class="teacher-tip">Teacher Tips are just wrappers for text. Be sure to include them in the javascript.</div>
        <a href="https://store.logicofenglish.com/products/foundations-a-complete-set" class="link-btn dark-blue-back white-text" loe-buy-now-btn>Buy Now</a>
    </div>
```