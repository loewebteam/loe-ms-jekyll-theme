strong big text for the start of a section

```html
<div class="hero-text">
    Here is some<br />
    <b>sweet</b> and <b>awesome</b> hero text!.
</div>
```