List boxes, may need some extra padding and margin, depending on how/when they're used.

```html
            <div class="list-box" style="width:auto;max-width:600px;margin:0 auto;padding:1em;">
                <h3>List Box</h3>
                <ul style="margin:0 auto;width:auto;">
                    <li>List stuff</li>
                    <li>In a list box.</li>
                </ul>
            </div>

```