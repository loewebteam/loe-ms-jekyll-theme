Buy now buttons pull the product identifier off the end of the href in order to build the product dialog.

```yaml
loeJsWidgets:
    - loe-buy-now
```

```html
    <a href="https://store.logicofenglish.com/products/foundations-a-complete-set" class="link-btn dark-blue-back white-text" loe-buy-now-btn>Buy Now</a>
```