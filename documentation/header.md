header for a page

```html
<div class="header">
    <h2>Phonograms</h2>
    <p class="headerDescription">Phono means sound and gram means picture.
        A phonogram is a picture of a sound.</p>
</div>
```