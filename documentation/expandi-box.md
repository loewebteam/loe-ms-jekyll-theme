set data height to restrict initial height of the div.

```yaml
loeJsWidgets: 
    - loe-expandi-box
```

```html
<div class="list-box" expandi data-height="200px">
    <h3>Training Benefits</h3>
    <ul>
        <li>40 hours of interactive literacy training at your fingertips.</li>
        <li>Practical tools to teach phonemic awareness, systematic phonics, fluency, vocabulary, and comprehension.</li>
        <li>Additional units on handwriting, spelling, composition, and grammar.</li>
        <li>Understand the neuroscience of reading.</li>
        <li>Learn how to implement Logic of English® Foundations and Essentials curricula.</li>
        <li>Facilitator’s Guide to lead a 4 day training.</li>
        <li>May qualify for CEUs! Check your state's requirements.</li>
    </ul>
</div>
```