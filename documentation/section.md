example for a standard section.

```html
    <div class="section">
        <h3 class="set-head">Section Head</h3>
        <div class="cell-content">
            <div class="hero-text">strong big text for the start of a section</div>
            <p>
                Here is a section, with cell content. Add a max-width, and the .cell-content element will keep it's contents to a maximum width.
            </p>

            more content...
        </div>
    </div>
```