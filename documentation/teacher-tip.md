Teacher Tips are just wrappers for text. Be sure to include them in the javascript.

```yaml
loeJsWidgets:
    - loe-teacher-tip
```


```html
    <div teacher-tip>Teacher Tips are just wrappers for text. Be sure to include them in the javascript.</div>
```